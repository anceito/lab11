package brass;

import java.util.List;
import java.util.Iterator;
import table.TableInterface;
import util.QueueLinked;

public class BrassBreadthFirstConnectionSearch extends BrassConnectionSearch implements BrassConnectionSearchInterface
{ 
	private BrassConnections brass_connections;
	public BrassBreadthFirstConnectionSearch(BrassConnections Bc){
		brass_connections=Bc;
	}
	public table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids){
		return brass_connections.breadthFirstConnections(start_city_id,comp_city_ids);
	}
}