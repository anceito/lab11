package brass;

import java.util.ArrayList;
import java.util.List;


public class BrassFindPlayersUnflippedCottonMills implements util.Command<BrassCity>
{ 
	private int player_id;
	private List<Integer> cotton_mill_city;
	
	
	public BrassFindPlayersUnflippedCottonMills(int p_id)
	{ 
		player_id = p_id;
		cotton_mill_city=new ArrayList<Integer>();
	}
	
	public List<Integer> getList()
	{ 
		return cotton_mill_city;
		
	}
	
	public void execute(BrassCity b_city)
	{
		BrassCountPlayersUnflippedIndustry count_player_unflipped_ind = new BrassCountPlayersUnflippedIndustry(BrassIndustryEnum.COTTON.getValue(), player_id);
		b_city.execute(count_player_unflipped_ind);
		int count_unflipped_ind = count_player_unflipped_ind.getCount();
		
		int cit_id = b_city.getCityID();
		
		if(count_unflipped_ind > 0)
			{ 
				cotton_mill_city.add(cit_id);
			}
	}
	
	
	
	
}