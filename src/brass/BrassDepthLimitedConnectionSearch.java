package brass;

import java.util.List;
import java.util.Iterator;
import table.TableInterface;

public class BrassDepthLimitedConnectionSearch extends BrassConnectionSearch implements BrassConnectionSearchInterface{ 
	private BrassConnections brass_connections;
	private int depth;
	public BrassDepthLimitedConnectionSearch(BrassConnections Bc,int depth_l){
		brass_connections=Bc;
		depth=depth_l;
	}
	public table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids){
		return brass_connections.depthLimitedConnections(start_city_id, depth,comp_city_ids);
	}
}
